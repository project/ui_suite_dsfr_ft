<?php

namespace Drupal\ui_suite_dsfr_ft\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\ui_suite_dsfr_ft\Plugin\Block\DisplayModalBlock;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;

/**
 * Defines a route controller for watches autocomplete form elements.
 */
class DisplaySvgAutocomplete extends ControllerBase {

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorage
   */
  protected $nodeStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->nodeStorage = $entity_type_manager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request) {
    $results = [];
    // Get the input searched.
    $input = $request->query->get('q');
    $input = Xss::filter($input);

    // Scan all the file of directory and subdirectories.
    $di = new \RecursiveDirectoryIterator(DisplayModalBlock::BASE_DIR);
    foreach (new \RecursiveIteratorIterator($di) as $filename => $file) {
      // We want only svg files.
      if ($file->getExtension() !== 'svg') {
        continue;
      }
      if (empty($input) || strstr($filename, $input) !== FALSE) {
        $results[] = [
          'value' => $filename,
          'label' => $filename,
        ];
      }
    }
    return new JsonResponse($results);
  }

}
