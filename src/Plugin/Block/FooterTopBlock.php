<?php

namespace Drupal\ui_suite_dsfr_ft\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Form\FormStateInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block with multiple menu rendered inside patterns.
 *
 * @Block(
 *   id = "ui_suite_dsfr_footer_top",
 *   admin_label = @Translation("Footer Top Menu"),
 *   category = @Translation("UI Suite DSFR"),
 * )
 */
class FooterTopBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
  protected $entityTypeManager;

  /** @var \Drupal\Core\Menu\MenuLinkTreeInterface */
  protected $menuTree;

  /** @var \Drupal\Core\Menu\MenuActiveTrailInterface */
  protected $menuActiveTrail;

  /**
   * Constructs a new Inea Menu block.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_tree
   *   The menu tree service.
   * @param \Drupal\Core\Menu\MenuActiveTrailInterface $menu_active_trail
   *   The active menu trail service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, EntityTypeManagerInterface $entity_type_manager, MenuLinkTreeInterface $menu_tree, MenuActiveTrailInterface $menu_active_trail,) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->menuTree = $menu_tree;
    $this->menuActiveTrail = $menu_active_trail;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('menu.link_tree'),
      $container->get('menu.active_trail')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $menus = $this->entityTypeManager->getStorage('menu')->loadMultiple();
    $form['menu'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => t('Menu'),
      '#required' => TRUE,
      '#options' => array_combine(array_keys($menus), array_keys($menus)),
      '#default_value' => $this->configuration['menu'] ?? [],
      '#ajax' => [
        'callback' => [$this, 'ajaxMenu'],
        'wrapper' => 'ajax-menu',
        'method' => 'replaceWith',
      ],
    ];
    self::buildTable($form, $form_state, $this->configuration, $menus);

    return $form;
  }

  /**
   * Ajax menu table order.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return bool
   */
  public static function ajaxMenu(array &$form, FormStateInterface $form_state) {
    return $form['settings']['menu_order'];
  }

  /**
   * Build the table settings form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param array $defaults
   * @param array $menus_liste
   *
   * @return void
   */
  private static function buildTable(array &$form, FormStateInterface $form_state, array $defaults, array $menus_liste) {
    $form['menu_order'] = [
      '#type' => 'details',
      '#tree' => TRUE,
      '#open' => TRUE,
      '#title' => t('Menu order'),
      '#description' => t('Order menu display. Set depth to 0 to not limit.'),
      '#prefix' => '<div id="ajax-menu">',
      '#suffix' => '</div>',
    ];

    $complete_form_state = $form_state instanceof SubformStateInterface ? $form_state->getCompleteFormState() : $form_state;
    $default_value = $defaults['order']['table'];
    $form['menu_order']['table'] = [
      '#type' => 'table',
      '#header' => [
        t('menu'),
        t('flatten'),
        t('level'),
        t('depth'),
        t('weight'),
      ],
      '#empty' => t('No menu added.'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
    ];
    $menu_select = $complete_form_state->getValue('settings');
    if (empty($menu_select['menu']) && !empty($default_value)) {
      $menu_select['menu'] = array_keys($default_value);
    }
    foreach ($menu_select['menu'] as $menu_name) {
      $form['menu_order']['table'][$menu_name]['#attributes']['class'][] = 'draggable';
      $form['menu_order']['table'][$menu_name]['#weight'] = $default_value[$menu_name]['weight'] ?? 0;
      $form['menu_order']['table'][$menu_name]['label'] = [
        '#plain_text' => $menus_liste[$menu_name] ? $menus_liste[$menu_name]->label() : $menu_name,
      ];
      $form['menu_order']['table'][$menu_name]['flatten'] = [
        '#type' => 'checkbox',
        '#title' => t('Flatten'),
        '#title_display' => 'invisible',
        '#delta' => $menu_name,
        '#attributes' => ['class' => ['ui_suite_dsfr_flatten_' . $menu_name]],
        '#default_value' => $default_value[$menu_name]['flatten'] ?? 0,
      ];
      $form['menu_order']['table'][$menu_name]['level'] = [
        '#type' => 'number',
        '#min' => 0,
        '#title' => t('Level'),
        '#title_display' => 'invisible',
        '#delta' => $menu_name,
        '#default_value' => $default_value[$menu_name]['level'] ?? 0,
      ];
      // Display depth only if flatten option is checked.
      $form['menu_order']['table'][$menu_name]['depth'] = [
        '#type' => 'number',
        '#min' => 0,
        '#title' => t('Depth'),
        '#title_display' => 'invisible',
        '#delta' => $menu_name,
        '#default_value' => $default_value[$menu_name]['depth'] ?? 0,
        '#states' => [
          // Show this textfield only if the radio 'other' is selected above.
          'visible' => [
            'input.ui_suite_dsfr_flatten_' . $menu_name => ['checked' => TRUE],
          ],
        ],
      ];
      $form['menu_order']['table'][$menu_name]['weight'] = [
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#title_display' => 'invisible',
        '#delta' => $menu_name,
        '#default_value' => $default_value[$menu_name]['weight'] ?? 0,
        '#attributes' => [
          'class' => [
            'table-sort-weight',
          ],
        ],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['menu'] = $form_state->getValue('menu');
    $this->configuration['order'] = $form_state->getValue('menu_order');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $menu = $this->configuration['menu'];
    $menu_order = $this->configuration['order']['table'];
    // Load all menus.
    $menus_load = $this->entityTypeManager->getStorage('menu')
      ->loadMultiple($menu);
    foreach ($menu_order as $menu_name => $menu_settings) {
      $build = $this->buildMenu($menu_name, $menu_settings);
      // Menu block ID.
      $build['#contextual_links']['menu'] = [
        'route_parameters' => ['menu' => $menu_name],
      ];
      $items[] = [
        'title' => $menus_load[$menu_name] ? $menus_load[$menu_name]->label() : $menu_name,
        'below' => $build['#items'],

      ];
    }
    $build_pattern['menu'] = [
      '#type' => 'pattern',
      '#id' => 'footer_top',
      '#fields' => ['items' => $items],
    ];

    return $build_pattern;
  }

  /**
   * Build the menu to render it.
   *
   * @param string $menu_name
   * @param array $menu_settings
   *
   * @return array
   */
  private function buildMenu(string $menu_name, array $menu_settings) {
    // Menu tree.
    $level = $menu_settings['level'];
    // Menu display depth.
    $depth = $menu_settings['depth'];
    $parameters = new MenuTreeParameters();
    /*
     * By not setting the any expanded parents we don't limit the loading of the
     * subtrees.
     * Calling MenuLinkTreeInterface::getCurrentRouteMenuTreeParameters we
     * would be doing so.
     * We don't actually need the parents expanded as we do different rendering.
     */
    $parameters->setMinDepth($level)
      ->setActiveTrail($this->menuActiveTrail->getActiveTrailIds($menu_name))
      ->onlyEnabledLinks();
    if ($depth && $menu_settings["flatten"]) {
      $maxdepth = min($level + ($depth - 1), $this->menuTree->maxDepth());
      $parameters->setMaxDepth($maxdepth);
    }
    $tree = $this->menuTree->load($menu_name, $parameters);
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    if ($menu_settings["flatten"]) {
      $manipulators[] = ['callable' => 'menu.default_tree_manipulators:flatten'];
    }
    $manipulators[] = ['callable' => 'menu.default_tree_manipulators:checkNodeAccess'];
    $tree = $this->menuTree->transform($tree, $manipulators);

    return $this->menuTree->build($tree);
  }

}

