<?php

namespace Drupal\ui_suite_dsfr_ft\Plugin\Block;

use Drupal\block\Entity\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a block with multiple menu rendered inside patterns.
 *
 * @Block(
 *   id = "ui_suite_dsfr_display_button",
 *   admin_label = @Translation("UI Suite dsfr display button"),
 *   category = @Translation("UI Suite DSFR"),
 * )
 */
class DisplayButtonBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $defaults = $this->getConfiguration();
    $form['label_button'] = [
      '#type' => 'textfield',
      '#default_value' => $defaults['label_button'] ?? t('Display settings'),
      '#required' => TRUE,
      '#title' => t('Label of link'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['label_button'] = $form_state->getValue('label_button');
    $complete_form_state = $form_state->getCompleteFormState();
    // Save the display region inside configuration.
    $this->configuration['display_region'] = $complete_form_state->getValue('region');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // $block = Block::load($block_id);
    $modal_id = 'fr-theme-modal';
    if (empty($this->configuration['display_region'])) {
      return [];
    }
    if (str_starts_with($this->configuration['display_region'], 'header')) {
      $button = [
        '#type' => 'pattern',
        '#id' => 'button',
        '#fields' => [
          'label' => $this->configuration['label_button'] ?? t('Display settings'),
        ],
        '#attributes' => [
          'class' => 'fr-icon-theme-fill',
          'data-fr-opened' => 'false',
          'aria-controls' => $modal_id,
        ],
      ];
    }

    if (str_starts_with($this->configuration['display_region'], 'footer')) {
      // We don't used the pattern cause we don't want the fr-btn class.
      $button = [
        '#type' => 'inline_template',
        '#template' => '<button class="fr-footer__bottom-link fr-icon-theme-fill fr-btn--icon-left">{{ label }}</button>',
        '#context' => [
          'label' => $this->configuration['label_button'] ?? t('Display settings'),
        ],
      ];
    }
    if (!empty($button)) {
      return [
        'button' => [
          '#theme' => 'item_list',
          '#attributes' => ['class' => 'fr-btns-group'],
          '#attached' => ['library' => 'ui_suite_dsfr_ft/dsfr-scheme'],
          '#items' => [
            [
              'value' => [
                $button,
              ],
            ],
          ],
        ],
      ];
    }
    return [];
  }

}

