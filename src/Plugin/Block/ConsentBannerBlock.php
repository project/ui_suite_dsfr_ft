<?php

namespace Drupal\ui_suite_dsfr_ft\Plugin\Block;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;


/**
 * Provides a block with multiple menu rendered inside patterns.
 *
 * @Block(
 *   id = "ui_suite_dsfr_consent_banner",
 *   admin_label = @Translation("Consent banner"),
 *   category = @Translation("UI Suite DSFR"),
 * )
 */
class ConsentBannerBlock extends BlockBase {

	/**
	 * {@inheritdoc}
	 */
	public function blockForm($form, FormStateInterface $form_state) {
		$form = parent::blockForm($form, $form_state);
		$defaults = $this->getConfiguration();
		$form['title'] = [
			'#type' => 'textfield',
			'#default_value' => $defaults['title'] ?? t('About cookies on sitename.fr'),
			'#required' => TRUE,
			'#title' => t('Title of modal'),
		];
		// Cannot use the checkboxes element cause the #states/required not working.
		$form['content'] = [
			'#type' => 'textarea',
			'#default_value' => $defaults['text'] ?? t('Welcome! We use cookies to improve your experience and the services available on this site. To find out more, visit the <a href="#">Personal data and cookies</a> page. You can, at any time, have control over the cookies you want to activate.'),
			'#required' => TRUE,
			'#title' => t('Body of modal'),
		];
		$form['modal_title'] = [
			'#type' => 'textfield',
			'#default_value' => $defaults['modal_title'] ?? t('Cookie management panel'),
			'#required' => TRUE,
			'#title' => t('Title of modal'),
		];
		$form['modal_text'] = [
			'#type' => 'textarea',
			'#default_value' => $defaults['modal_text'] ?? t('Preferences for all services'),
			'#required' => TRUE,
			'#title' => t('Body of modal'),
		];
		$form['services'] = [
			'#type' => 'details',
			'#tree' => TRUE,
			'#open' => TRUE,
			'#title' => t('Services'),
			//   '#description' => $description,
			'#prefix' => '<div id="ui-suite-dsfr-ft-services">',
			'#suffix' => '</div>',
		];

		$triggering_element = $form_state->getTriggeringElement();

		$services = $defaults['services'] ?? [];
		$default_count = 0;
		if (!empty($services)) {
			uasort($services, [SortArray::class, 'sortByWeightElement']);
			$default_count = count($services);
		}
		$count_service = $form_state->get('count_server') ?? $default_count;
		// Trigger ajax or default value.
		if (!empty($triggering_element['#name']) && $triggering_element['#name'] === 'add_service') {
			$count_service++;
			$form_state->set('count_server', $count_service);
		}
		$delta_key_remove = -1;
		if (!empty($triggering_element['#name']) && strpos($triggering_element['#name'], 'remove_service_') === 0) {
			$triger_explode = explode('_', $triggering_element['#name']);
			$delta_key_remove = $triger_explode[2] ?? -1;
		}
		$form['services']['table'] = [
			'#type' => 'table',
			'#header' => [
				$this->t('Title'),
				$this->t('ID'),
				$this->t('Link title'),
				$this->t('Remove'),
				$this->t('Weight'),
			],
			'#empty' => $this->t('No servers added.'),
			'#tabledrag' => [
				[
					'action' => 'order',
					'relationship' => 'sibling',
					'group' => 'table-sort-weight',
				],
			],
		];
		// Add our names fields.
		for ($delta = 0; $delta < $count_service; $delta++) {
			if ($delta_key_remove == $delta) {
				continue;
			}
			$form['services']['table'][$delta]['#attributes']['class'][] = 'draggable';
			$form['services']['table'][$delta]['#weight'] = $services[$delta]['weight'] ?? 0;
			$form['services']['table'][$delta]['title'] = [
				'#type' => 'textfield',
				'#default_value' => $services[$delta]['title'] ?? '',
				'#required' => TRUE,
			];
			$form['services']['table'][$delta]['id'] = [
				'#type' => 'textfield',
				'#default_value' => $services[$delta]['id'] ?? '',
				'#required' => TRUE,
			];
			$form['services']['table'][$delta]['link_title'] = [
				'#type' => 'textfield',
				'#default_value' => $services[$delta]['link_title'] ?? '',
			];

			$form['services']['table'][$delta]['remove'] = [
				'#type' => 'button',
				'#value' => $this->t('Remove'),
				'#name' => 'remove_service_' . $delta,
			];
			$form['services']['table'][$delta]['weight'] = [
				'#type' => 'weight',
				'#title' => t('Weight'),
				// '#title_display' => 'invisible',
				'#default_value' => $services[$delta]['weight'] ?? $delta,
				'#attributes' => [
					'class' => [
						'table-sort-weight',
					],
				],
			];
		}
		if (!empty($triggering_element['#name']) && strpos($triggering_element['#name'], 'remove_service_') === 0) {
			$count_service--;
			if ($count_service < 0) {
				$count_service = 0;
			}
			$form_state->set('count_server', $count_service);
		}
		// Button to add more names.
		$form['services']['add_service'] = [
			'#type' => 'button',
			'#name' => 'add_service',
			'#value' => $this->t('Add service'),
			'#ajax' => [
				'callback' => [$this, 'ajaxService'],
				'wrapper' => 'ui-suite-dsfr-ft-services',
				'method' => 'replaceWith',
			],
		];

		return $form;
	}

	/**
	 * Generate bundle form options.
	 *
	 * @param array $form
	 * @param \Drupal\Core\Form\FormStateInterface $form_state
	 *
	 */
	public static function ajaxService(array &$form, FormStateInterface $form_state) {
		// $form_state->setRebuild();
		return $form['settings']['services'];
	}

	/**
	 * Ajax callback to add field.
	 *
	 * {@inheritdoc}
	 */
	public function ajaxAddService(array &$form, FormStateInterface $form_state) {
		return $form['services'];
	}

	/**
	 * {@inheritdoc}
	 */
	public function blockSubmit($form, FormStateInterface $form_state) {
		$services = $form_state->getValue('services');
		$this->configuration['services'] = $services['table'] ?? [];
		$this->configuration['title'] = $form_state->getValue('title');
		$this->configuration['content'] = $form_state->getValue('content');
		$this->configuration['modal_title'] = $form_state->getValue('modal_title');
		$this->configuration['modal_text'] = $form_state->getValue('modal_text');
	}

	/**
	 * {@inheritdoc}
	 */
	public function build() {
		$services = $this->configuration['services'];
		$pattern_services = [];
		foreach ($services as $service) {
			$pattern_services[] = ['title' => $service['title'],
			                       'attributes' => [
				                       'id' => $service['id'],
				                       'title' => $service['link_title'],
			                       ],
			];
		}
		$build_pattern['menu'] = [
			'#type' => 'pattern',
			'#id' => 'consent_banner',
			'#fields' => [
				'title' => $this->configuration['title'] ?? t('About cookies on sitename.fr'),
				'content' => $this->configuration['content'] ?? t('Welcome! We use cookies to improve your experience and the services available on this site. To find out more, visit the <a href="#">Personal data and cookies</a> page. You can, at any time, have control over the cookies you want to activate..'),
				'modal_title' => $this->configuration['modal_title'] ?? t('Cookie management panel'),
				'modal_text' => $this->configuration['modal_text'] ?? t('Preferences for all services'),
			],
			'#settings' => [
				'services' => $pattern_services,
			]
		];

		return $build_pattern;
	}

}

