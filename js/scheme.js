const applyTheme = (mode) => {
    if (mode === "system") {
        console.log("Set theme if set in prefers-color-scheme");
        window.matchMedia("(prefers-color-scheme: dark)").matches && applyTheme('dark');
        window.matchMedia("(prefers-color-scheme: light)").matches && applyTheme('light');
    } else {
        document.documentElement.setAttribute("data-fr-theme", mode), (document.documentElement.style.colorScheme = mode);
        console.log(`Set ${mode} theme`);
    }
};
(() => {
    if (document.documentElement.matches(`:root[data-fr-theme], :root[data-fr-scheme]`)) {
        const schemeLocStorage = (() => {
                try {
                    return "localStorage" in window && null !== window.localStorage;
                } catch (e) {
                    return !1;
                }
            })()
                ? localStorage.getItem("scheme")
                : "",
            schemeFromMarkup = document.documentElement.getAttribute('data-fr-scheme');
        console.log("scheme from localstorage=", schemeLocStorage);
        console.log("scheme from html markup=", schemeFromMarkup);
        switch (!0) {
            case schemeLocStorage !== "":
                console.log(`local storage explicit ${schemeLocStorage}`);
                applyTheme(schemeLocStorage);
                break;
            case schemeFromMarkup !== "":
                console.log(`html markup explicit ${schemeFromMarkup}`);
                applyTheme(schemeFromMarkup);
                break;
        }
    }
})();
