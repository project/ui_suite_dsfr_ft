<?php

/**
 * @file
 */

/**
 * Implements hook_requirements().
 */
function ui_suite_dsfr_ft_requirements($phase) {
  if (!\Drupal::service('theme_handler')->themeExists('ui_suite_dsfr')) {
    $requirements['theme_enable'] = [
      'description' => t('Theme UI Suite DSFR needs to be enable. <a href=":url">UI Suite DSFR</a>.', [
        ':url' => 'https://www.drupal.org/project/ui_suite_dsfr',
      ]),
      'severity' => REQUIREMENT_ERROR,
      'value' => t('Theme not enabled'),
      'title' => t('Theme UI suite DSFR'),
    ];

    return $requirements;
  }
  $version = \Drupal::service('theme_handler')
    ->getTheme('ui_suite_dsfr')->info['version'];
  [$version_nuber, $pre_release] = array_pad(explode('-', $version), 2, null);
  $version_explode = explode('.', $version_nuber);
  if ((float) $version_explode[0] > 1.0) {
    return [];
  }

  if (!empty($pre_release)) {
    if (!str_starts_with($pre_release, 'rc') && !str_contains($pre_release, "dev")) {
      $requirements['theme_version'] = [
        'description' => t('Theme UI Suite DSFR needs to have version upper to 1.0.0-rc3 (version installed @vers).', ['@vers' => $version]),
        'severity' => REQUIREMENT_ERROR,
        'value' => t('Theme version'),
        'title' => t('Theme UI suite DSFR'),
      ];
      return $requirements;
    }
    $numbers = preg_replace('/[^0-9]/', '', $pre_release);
    if ($numbers <= 3) {
      $requirements['theme_version'] = [
        'description' => t('Theme UI Suite DSFR needs to have version newer than 1.0.0-rc3 (version installed @vers).', ['@vers' => $version]),
        'severity' => REQUIREMENT_ERROR,
        'value' => t('Theme version'),
        'title' => t('Theme UI suite DSFR'),
      ];
      return $requirements;
    }
  }
  return [];
}
